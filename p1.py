
"""
p1 ) sum of all the natural numbers less than 1000 that are multiple of 3 or 5 
"""

print sum(i for i in range(1000) if (i % 3 == 0) or (i % 5 == 0))
