"""
p 14) The following iterative sequence is defined for the set of positive integers:

n -> n/2 (n is even)
n -> 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1

Which starting number, under one million, produces the longest chain?
"""
cache = {1:1} 

def collatz_length(n):
    """Return length of the collatz sequence for n."""
    length = 1
    while n > 1:
        if n in cache:
            return length + cache[n]
        if n % 2 == 0:
            n = n//2
        else:
            n = 3*n + 1
        length += 1
    return length

longest = 1
for n in range(1000000):
    length = collatz_length(n)
    cache[n] = length
    if length > cache[longest]:
        longest = n
print(longest)

