"""
p 9 ) A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a^2 + b^2 = c^2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

"""


from math import sqrt

for b in range(500):
    for a in range(b):
        c = int(sqrt(a*a + b*b))
        x = sqrt(a*a + b*b)
        if c == x and a + b + c == 1000:
            print(a*b*c)
            exit()
