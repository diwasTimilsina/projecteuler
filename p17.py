"""
 problem 17) If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
             If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
""" 

import sys
import os
import time

#ones stores the length of words in ones's place
# and tens stores the length of words in ten's place
ones = [0,3,3,5,4,4,3,5,5,4]
teens = [3,6,6,8,8,7,7,9,8,8]
tens = [0,3,6,6,5,5,5,7,6,6]
hundred_length = 7
thousand_length = 11
count = 0

# compute the sum 
def computeSum(num, sumSoFar):
    if num < 10:
        sumSoFar += ones[num]
        return sumSoFar
    if num >= 10 and num < 100:
        if (num < 20):
            return sumSoFar+teens[num-10]
        else:
            sumSoFar += tens[num/10]
            return computeSum(num%10,sumSoFar)
    if num >= 100 and num < 1000:
        sumSoFar += ( hundred_length+ones[num/100])
        if (num % 100 != 0) :
            sumSoFar +=3
        return computeSum(num%100,sumSoFar)

start = time.time()
print sum(computeSum(x,0) for x in range(1,1000)) + thousand_length
end = time.time()    
print ("total time = {:.5f}".format(end-start))
