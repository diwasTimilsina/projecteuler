"""
p 15) Starting in the top left corner of a 2x2 grid, and only being able to move to the right and down, 
there are exactly 6 routes to the bottom right corner.
How many such routes are there through a 20x20 grid?
"""

def fact(n):
    return 1 if n == 1 else n*fact(n-1)  

def chose(n,k):
    return fact(n)/(fact(k)*fact(n-k))

print(chose(40,20))
