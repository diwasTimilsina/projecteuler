"""
 p 21) Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a  b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000. 
"""

import time 
import math
def computeProperDivisor(num):
    divisor_list = [1]
    for i in range(2,int(math.sqrt(num)+1)):
        if num % i == 0:
            divisor_list.append(i)
            divisor_list.append(num/i)
    return divisor_list


start = time.time()
amicableSum = 0
amicable = []

divisor_sum = sum(x for x in computeProperDivisor(220))
another_divisor_sum = sum(y for y in computeProperDivisor(divisor_sum))

for j in range(10,10001):
    divisor_sum = sum(x for x in computeProperDivisor(j))
    another_divisor_sum = sum(y for y in computeProperDivisor(divisor_sum))
    if another_divisor_sum == j and not divisor_sum == another_divisor_sum:
        amicableSum+=j
        
print amicableSum
end = time.time()
print ("total time: {:.5f}".format(end-start))

