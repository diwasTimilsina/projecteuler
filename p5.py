"""
p 5) 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

"""

import time
from commonFunctions import factorizeWithRepeats 

# compute the smallest integer that is evenly divisible by 
# all from 1,2,3,4......,num-1,num
def smallestEvenlyDivisibleInt(num):
    factorList = []
    for i in range(1,num + 1):
        factorList = factor_append(factorList, factorizeWithRepeats(i))
    product = 1
    for i in factorList: 
        product *= i
    return product


# merge two lists of factors  
def factor_append(factors,newFactors):
    if len(factors) == 0:
        return newFactors
    for i in range(len(newFactors)):
        if i > 0 and newFactors[i] == newFactors[i-1]: continue
        newCount = newFactors.count(newFactors[i])
        oldCount = factors.count(newFactors[i])
       
        if newCount > oldCount:
            for j in range(newCount - oldCount): 
                factors.append(newFactors[i])
    factors.sort()
    return factors

start = time.time()
print(smallestEvenlyDivisibleInt(20))
end = time.time()
print (end- start)
