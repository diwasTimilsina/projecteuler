"""
P 8) Compute the 13 digits with the greates product in a given list of numbers
"""


import sys
import time 


start = time.time()


with open(sys.argv[1]) as f:
    lines = f.readlines()

numList = []  
for line in lines:

    numList.extend(int(line[i]) for i in range(len(line)) if not line[i] == "\n")

def multiplyList(lst):
    p = 1
    for n in lst :
        p *=n
    return p

# compute product
prod = 0
for i in range(len(numList)-12):
    p = multiplyList(numList[i:i+13])
    if p > prod: prod = p

print prod
end = time.time() 

print "time: %s seconds" % (end-start)
