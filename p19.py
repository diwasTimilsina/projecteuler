"""
You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

"""

import time 

week_count = 2
month_length = [31,28,31,30,31,30,31,31,30,31,30,31]
total_sundays = 0
for year in range(1,101):
    leap_year = False
    if year % 4 == 0:
        leap_year = True
    for i in range(len(month_length)):
        total_days = month_length[i]
        if leap_year and (i == 1):
            total_days = 29
        for day in range(total_days):
            if day == 0 and day == week_count:
                total_sundays +=1
            week_count = (week_count+1) % 7
            
start = time.time()
print total_sundays
end = time.time()
print ("total time: {0}".format(end- start))
